#include "CollisionSystem.h"

#define CIRCLES_PENETRATION_FACTOR 0.4f
#define RECTANGLES_PENETRATION_FACTOR 0.01f

void CollisionSystem::update(float dt)
{
	for (size_t i = 0; i < _circles.size(); ++i)
	{
		CircleShape *lshape = _circles[i];

		for (size_t j = 0; j < _circles.size(); ++j)
		{
			CircleShape *rshape = _circles[j];
		
			float penetration = 0.0f;
			if (lshape != rshape && circleVsCircleCollision(lshape, rshape, penetration))
			{
				vec2f collision_normal = normalize(lshape->position - rshape->position);

				const float percent = CIRCLES_PENETRATION_FACTOR;
				vec2f correction = penetration / (1.0f / lshape->mass + 1.0f / rshape->mass) * percent * collision_normal;
				lshape->position -= lshape->mass * correction;
				lshape->position += rshape->mass * correction;

				resolveCirclesCollision(lshape, rshape);
			}
		}
		
		for (size_t k = 0; k < _rectangles.size(); ++k)
		{
			RectangleShape *rectangle = _rectangles[k];

			vec2f collision_normal, collision_point;
			float penetration = 0.0f;
			if (
				circleVsRectCollision(
					lshape,
					rectangle,
					collision_point,
					penetration
					))
			{
				lshape->position -= normalize(lshape->position - collision_point) * penetration * RECTANGLES_PENETRATION_FACTOR;

				resolveCircleVSRectCollision(lshape, rectangle, collision_point);
			}
		}
	}

	applyGravity(_circles, dt);
}

bool CollisionSystem::checkCollisionWithRectangles(CircleShape *circle)
{
	for (size_t i = 0; i < _rectangles.size(); ++i)
	{
		RectangleShape *rectangle = _rectangles[i];
		vec2f collision_normal, collision_point;
		float penetration = 0.0f;
		if (
			circleVsRectCollision(
				circle,
				rectangle,
				collision_point,
				penetration
				))
		{
			return true;
		}
	}
	return false;
}

void CollisionSystem::addCircle(CircleShape *shape)
{
	_circles.push_back(shape);
}

void CollisionSystem::addRectangle(RectangleShape *shape)
{
	_rectangles.push_back(shape);
}

void CollisionSystem::removeAllCircles()
{
	_circles.clear();
}

void CollisionSystem::removeAllRectangles()
{
	_rectangles.clear();
}

void CollisionSystem::clear()
{
	removeAllCircles();
	removeAllRectangles();
}

void CollisionSystem::setDisplaySize(uint16_t width, uint16_t height)
{
	_screen_w = width;
	_screen_h = height;
}

bool CollisionSystem::circleVsRectCollision(
	CircleShape *c,
	RectangleShape *r,
	vec2f &collision_point,
	float &penetration)
{
	vec2f rect_center = r->centerPosition();
	vec2f n = c->position - rect_center;

	vec2f closest = n;

	float x_extent = (r->max.x - r->min.x) / 2;
	float y_extent = (r->max.y - r->min.y) / 2;

	closest.x = clamp(-x_extent, x_extent, closest.x);
	closest.y = clamp(-y_extent, y_extent, closest.y);

	bool inside = false;
	
	if (n == closest)
	{
		inside = true;

		if (abs(n.x) > abs(n.y))
		{
			if (closest.x > 0)
				closest.x = x_extent;
			else
				closest.x = -x_extent;
		}

		else
		{
			if (closest.y > 0)
				closest.y = y_extent;
			else
				closest.y = -y_extent;
		}
	}
	
	vec2f normal = n - closest;
	float d = normal.x * normal.x + normal.y * normal.y;
	float radius = c->radius;

	if ( (d > (radius * radius) ) && !inside)
	{
		return false;
	}
	if (inside)
	{
		penetration = -(radius - d);
	}
	else
	{
		penetration = radius - d;
	}
	collision_point = closest;

	return true;
}

void CollisionSystem::resolveCircleVSRectCollision(CircleShape *c, RectangleShape *r, const vec2f &collision_point)
{
	vec2f rv = c->velocity - r->velocity;

	vec2f normal = normalize(collision_point - c->position);
	float velAlongNormal = dot(rv, normal);

	if (velAlongNormal > 0)
	{
		return;
	}

	float e = (c->restitution < r->restitution) ? c->restitution : r->restitution;

	float j = (1 + e) * velAlongNormal;
	j /= 1 / c->mass + 1 / r->mass;

	vec2f impulse = j * normal;
	c->velocity -= 1 / c->mass * impulse;
}

bool CollisionSystem::circleVsCircleCollision(CircleShape *l, CircleShape *r, float &penetration)
{
	float x = l->position.x - r->position.x;
	float y = l->position.y - r->position.y;
	float centerDistanceSq = x * x + y * y;
	float radius = l->radius + r->radius;
	float radiusSq = radius * radius;

	bool collision = centerDistanceSq <= radiusSq;
	if (collision)
	{
		penetration = l->radius + r->radius - sqrt(centerDistanceSq);
	}

	return collision;
}

void CollisionSystem::resolveCirclesCollision(CircleShape *l, CircleShape *r)
{
	vec2f rv = l->velocity - r->velocity;

	vec2f normal = normalize(l->position - r->position);
	float velAlongNormal = dot(rv, normal);

	if (velAlongNormal > 0)
	{
		return;
	}

	float e = (l->restitution < r->restitution) ? l->restitution : r->restitution;

	float j = (1 + e) * velAlongNormal;
	j /= 1.0f / l->mass + 1.0f / r->mass;

	vec2f impulse = j * normal;
	l->velocity -= 1 / l->mass * impulse * 0.2f;
	r->velocity += 1 / r->mass * impulse * 0.2f;
}