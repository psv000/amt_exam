#pragma once

#include "CircleShape.h"

class Circle
{
public:

	void draw			();
	void setPosition	(float x, float y);
	void setRadius		(float radius);
	float radius		() const;

	CircleShape *shape();

private:
	CircleShape _shape;
};

