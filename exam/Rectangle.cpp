#include "Rectangle.h"
#include <Gl/gl.h>

#define GLASS_COLOUR_RED	1.0f
#define GLASS_COLOUR_GREEN	0.0f
#define GLASS_COLOUR_BLUE	0.0f

void Rect::draw()
{
	glBegin(GL_TRIANGLES);
	glColor3f(GLASS_COLOUR_RED, GLASS_COLOUR_GREEN, GLASS_COLOUR_BLUE);

	glVertex2f(_shape.min.x, _shape.min.y);
	glVertex2f(_shape.max.x, _shape.min.y);
	glVertex2f(_shape.max.x, _shape.max.y);
	glVertex2f(_shape.max.x, _shape.max.y);
	glVertex2f(_shape.min.x, _shape.max.y);
	glVertex2f(_shape.min.x, _shape.min.y);

	glEnd();
}

void Rect::setPosition(float x, float y)
{
	_shape.min.x = x;
	_shape.min.y = y;
}

void Rect::setSize(float width, float height)
{
	_shape.max.x = width + _shape.min.x;
	_shape.max.y = height + _shape.min.y;
}

RectangleShape *Rect::shape()
{
	return &_shape;
}