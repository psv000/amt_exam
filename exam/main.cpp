
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <stdio.h>
#include <ctime>

#include "Scene.h"

Scene scene;

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	scene.draw();
	glFlush();
}


LONG WINAPI WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static PAINTSTRUCT ps;

	switch (uMsg) {
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_SPACE:
			scene.clear();
			break;
		default:
			break;
		}
	case WM_PAINT:
		display();
		BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		return 0;
	case WM_MOUSEWHEEL:
	{
		int delta = GET_WHEEL_DELTA_WPARAM(wParam);
		scene.resizeNewCircle(delta * 0.00005f);
		InvalidateRect(hWnd, NULL, true);
		break;
	}
	case WM_LBUTTONDOWN:
	{
		RECT window_rect;
		if (GetClientRect(hWnd, &window_rect)) {
			float x = static_cast<float>(LOWORD(lParam)) / (window_rect.right - window_rect.left) * 2.0f - 1.0f;
			float y = static_cast<float>(HIWORD(lParam)) / (window_rect.top - window_rect.bottom) * 2.0f + 1.0f;

			scene.tryAddNewCircle(x, y);
			InvalidateRect(hWnd, NULL, true);
		}
	}
	case WM_MOUSEMOVE:
	{
		RECT window_rect;
		if (GetClientRect(hWnd, &window_rect)) {
			float x = static_cast<float>(LOWORD(lParam)) / (window_rect.right - window_rect.left) * 2.0f - 1.0f;
			float y = static_cast<float>(HIWORD(lParam)) / (window_rect.top - window_rect.bottom) * 2.0f + 1.0f;

			scene.setNewCirclePosition(x, y);
			InvalidateRect(hWnd, NULL, true);
		}
		break;
	}
	case WM_LBUTTONUP:
		scene.launchNewCircle();
		break;
	case WM_SIZE:
		scene.resize(LOWORD(lParam), HIWORD(lParam));
		glViewport(0, 0, LOWORD(lParam), HIWORD(lParam));
		PostMessage(hWnd, WM_PAINT, 0, 0);
		return 0;

	case WM_CHAR:
		switch (wParam) {
		case 27:
			PostQuitMessage(0);
			break;
		}
		return 0;

	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

HWND CreateOpenGLWindow(LPCWSTR title, int x, int y, int width, int height,
	BYTE type, DWORD flags)
{
	int         pf;
	HDC         hDC;
	HWND        hWnd;
	WNDCLASS    wc;
	PIXELFORMATDESCRIPTOR pfd;
	static HINSTANCE hInstance = 0;

	if (!hInstance) {
		hInstance = GetModuleHandle(NULL);
		wc.style = CS_OWNDC;
		wc.lpfnWndProc = (WNDPROC)WindowProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hInstance;
		wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = NULL;
		wc.lpszMenuName = NULL;
		wc.lpszClassName = L"OpenGL";

		if (!RegisterClass(&wc)) {
			MessageBox(NULL, L"RegisterClass() failed:  "
				"Cannot register window class.", L"Error", MB_OK);
			return NULL;
		}
	}

	auto styles = WS_OVERLAPPEDWINDOW & ~WS_THICKFRAME & ~WS_MAXIMIZEBOX | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	hWnd = CreateWindow(L"OpenGL", title, styles, x, y, width, height, NULL, NULL, hInstance, NULL);

	if (hWnd == NULL) {
		MessageBox(NULL, L"CreateWindow() failed:  Cannot create a window.", L"Error", MB_OK);
		return NULL;
	}

	hDC = GetDC(hWnd);

	memset(&pfd, 0, sizeof(pfd));
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | flags;
	pfd.iPixelType = type;
	pfd.cColorBits = 32;

	pf = ChoosePixelFormat(hDC, &pfd);
	if (pf == 0) {
		MessageBox(NULL, L"ChoosePixelFormat() failed:  "
			"Cannot find a suitable pixel format.", L"Error", MB_OK);
		return 0;
	}

	if (SetPixelFormat(hDC, pf, &pfd) == FALSE) {
		MessageBox(NULL, L"SetPixelFormat() failed:  "
			"Cannot set format specified.", L"Error", MB_OK);
		return 0;
	}

	DescribePixelFormat(hDC, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

	ReleaseDC(hWnd, hDC);

	return hWnd;
}

int APIENTRY WinMain(HINSTANCE hCurrentInst, HINSTANCE hPreviousInst, LPSTR lpszCmdLine, int nCmdShow)
{
	HDC hDC;
	HGLRC hRC;
	HWND  hWnd;
	MSG   msg;

	hWnd = CreateOpenGLWindow(L"exam", 0, 0, 512, 512, PFD_TYPE_RGBA, 0);
	if (hWnd == NULL)
		exit(1);

	hDC = GetDC(hWnd);
	hRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, hRC);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	bool terminated = false;

	while (!terminated) {
		while (PeekMessage(&msg, hWnd, 0, 0, PM_NOREMOVE)) {
			if (GetMessage(&msg, hWnd, 0, 0)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else {
				terminated = true;

				wglMakeCurrent(NULL, NULL);
				ReleaseDC(hWnd, hDC);
				wglDeleteContext(hRC);
				DestroyWindow(hWnd);
				
				break;				
			}
		}
		display();
	}	
	return msg.wParam;
}
