#pragma once

#include "Vector.h"
#include "CircleShape.h"
#include "RectangleShape.h"

class CollisionSystem
{
public:
	CollisionSystem() = default;
	CollisionSystem(const CollisionSystem &) = delete;
	CollisionSystem &operator=(const CollisionSystem &) = delete;
	
	void update(float dt);

	void addCircle		(CircleShape *);
	void addRectangle	(RectangleShape *);

	void removeAllCircles();
	void removeAllRectangles();

	void clear();

	void setDisplaySize(uint16_t width, uint16_t height);
	bool checkCollisionWithRectangles(CircleShape *);

private:

	template<class T>
	void applyGravity(Vector<T *> &vector, float dt);

	bool circleVsRectCollision(
		CircleShape *c,
		RectangleShape *r,
		vec2f &collision_point,
		float &penetration
	);

	void resolveCircleVSRectCollision(CircleShape *c, RectangleShape *r, const vec2f &collision_point);

	bool circleVsCircleCollision(CircleShape *l, CircleShape *r, float &penetration);
	void resolveCirclesCollision(CircleShape *l, CircleShape *r);

private:
	using Circles		= Vector<CircleShape *>;
	using Rectangles	= Vector<RectangleShape *>;

	Circles		_circles;
	Rectangles	_rectangles;

	uint16_t _screen_w{ 0 }, _screen_h { 0 };

	const vec2f GRAVITY_ACCELERATION { 0.0f, -0.5f };
};

template<class T>
void CollisionSystem::applyGravity(Vector<T *> &vector, float dt)
{
	for (size_t i = 0; i < vector.size(); ++i)
	{
		T *object = vector[i];
		if (object->free)
		{
			vec2f &velocity = object->velocity;
			vec2f &position = object->position;
			velocity.x += GRAVITY_ACCELERATION.x * dt / _screen_w;
			velocity.y += GRAVITY_ACCELERATION.y * dt / _screen_h;

			position += velocity * dt;
		}
	}
}