#pragma once

#include <memory.h>
#include <cmath>
#include <inttypes.h>

template<typename T>
struct vec2 {
	union {
		T vec[2];
		struct {
			T x, y;
		};
	};

	vec2(T x, T y) :
		x(x),
		y(y) {

	}
	vec2(T v) : vec{ v, v } {

	}
	vec2() {
		memset(vec, 0, sizeof(T) * 2);
	}
	T operator[](int i) const { return vec[i % 2]; }
	T &operator[](int i) { return vec[i % 2]; }
	vec2<T> &operator*=(const T &v) {
		this->x *= v;
		this->y *= v;
		return *this;
	}

	vec2<T> &operator+=(const vec2<T> &vec)
	{
		this->x += vec.x;
		this->y += vec.y;
		return *this;
	}

	vec2<T> &operator-=(const vec2<T> &vec)
	{
		this->x -= vec.x;
		this->y -= vec.y;
		return *this;
	}
};

	
template<typename T>
vec2<T> operator*(const vec2<T> &vec, const T &v) {
	return vec2<T>(v * vec.x, v * vec.y);
}

template<typename T>
vec2<T> operator*(const T &v, const vec2<T> &vec) {
	return vec * v;
}

template<typename T>
vec2<T> operator-(const vec2<T> &r, const vec2<T> &l) {
	vec2<T> v;
	v.x = r.x - l.x;
	v.y = r.y - l.y;
	return v;
}

#define DEFAULT_COMP_DELTA 0.000000001
template<typename T>
bool operator==(const vec2<T> &r, const vec2<T> &l) {
	double x = fabs((double)r.x - (double)l.x);
	double y = fabs((double)r.y - (double)l.y);
	return x < DEFAULT_COMP_DELTA && y < DEFAULT_COMP_DELTA;
}

using vec2f = vec2<float>;

template<typename T>
inline float dot(const vec2<T> &l, const vec2<T> &r)
{
	return l.x * r.x + l.y * r.y;
}

template<typename T>
inline vec2<T> normalize(const vec2<T> &vec)
{
	float i_l = 1.0f / sqrt(vec.x*vec.x + vec.y*vec.y);
	return vec * i_l;
}

template<typename T>
inline T clamp(const T &min, const T &max, const T &val)
{
	return (val < min) ? min : (val > max) ? max : val;
}