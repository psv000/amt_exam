#pragma once

#include "RectangleShape.h"

class Rect
{
public:
	
	void draw();
	void setPosition(float x, float y);
	void setSize(float width, float height);

	RectangleShape *shape();

private:
	RectangleShape _shape;
};