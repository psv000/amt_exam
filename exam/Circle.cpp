#include "Circle.h"
#include <GL/gl.h>
#define _USE_MATH_DEFINES
#include <math.h>

#define CIRCLE_COLOUR_RED	0.0f
#define CIRCLE_COLOUR_GREEN	1.0f
#define CIRCLE_COLOUR_BLUE	0.0f

#define RASTERIZATION_FACTOR 720

void Circle::draw()
{
	glBegin(GL_LINE_LOOP);
	glColor3f(CIRCLE_COLOUR_RED, CIRCLE_COLOUR_GREEN, CIRCLE_COLOUR_BLUE);
	uint16_t points_count = static_cast<int>(RASTERIZATION_FACTOR * _shape.radius);
	for (uint16_t i = points_count; i > 0; --i)
	{
		float degInRad = i * (2.0f / points_count) * M_PI;
		glVertex2f(
			_shape.position.x + cos(degInRad) * _shape.radius,
			_shape.position.y + sin(degInRad) * _shape.radius
		);
	}
	glEnd();
}

void Circle::setPosition(float x, float y)
{
	_shape.position.x = x;
	_shape.position.y = y;
}

void Circle::setRadius(float radius)
{
	_shape.radius = radius;
}

float Circle::radius() const
{
	return _shape.radius;
}

CircleShape *Circle::shape()
{
	return &_shape;
}