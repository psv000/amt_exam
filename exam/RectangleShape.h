#pragma once

#include "vec2.h"

struct RectangleShape
{
	vec2f min, max;
	vec2f velocity;
	float restitution { 2.0f };
	float mass { 1.0f };

	vec2f centerPosition() const
	{
		return vec2f( (max.x + min.x), (max.y + min.y) ) * 0.5f;
	}
};