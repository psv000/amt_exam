#include "Scene.h"
#include "ArraySizeHelper.h"
#include <GL/gl.h>
#include <ctime >

#define DEFAULT_CIRCLE_SIZE 0.1f
#define MIN_CIRCLE_SIZE 0.01f
#define MAX_CIRCLE_SIZE 0.3f

#define CS_UPDATE_DELTA 0.016f

Scene::Scene():
_last_update_time(0.0),
_new_circle_size(DEFAULT_CIRCLE_SIZE),
_width(0),
_height(0),
_new_circle(nullptr)
{

	float rect_positions[] = {
		-1.0f, -1.0f,
		-1.0f, -0.7f,
		 0.7f, -0.7f
	};

	float rect_sizes[] = {
		2.0f, 0.3f,
		0.3f, 1.7f,
		0.3f, 1.7f
	};

	if (size_of_array(rect_positions) == size_of_array(rect_sizes))
	{
		for (uint8_t i = 0; i < size_of_array(rect_positions) / 2; ++i)
		{
			_rectangles.push_back(new Rect());
			Rect *rect = _rectangles.back();
			rect->setPosition(rect_positions[i * 2], rect_positions[i * 2 + 1]);
			rect->setSize(rect_sizes[i * 2], rect_sizes[i * 2 + 1]);

			_collision_system.addRectangle(rect->shape());
		}
	}
}

Scene::~Scene()
{
	_collision_system.clear();

	delete _new_circle;
	_new_circle = nullptr;

	_circles_on_scene.clear();
	_rectangles.clear();
}

template<class T>
void drawObjects(Vector<T> &vector)
{
	for (size_t i = 0; i < vector.size(); ++i)
	{
		vector[i]->draw();
	}
}

void Scene::draw()
{
	_collision_system.update(CS_UPDATE_DELTA);
	
	drawObjects(_rectangles);
	drawObjects(_circles_on_scene);
	if (_new_circle)
	{
		_new_circle->draw();
	}

}

void Scene::tryAddNewCircle(float x, float y)
{
	delete _new_circle;
	
	_new_circle = new Circle();
	_new_circle->setRadius(_new_circle_size);
	_new_circle->setPosition(x, y);

	if (_collision_system.checkCollisionWithRectangles( _new_circle->shape()) )
	{
		delete _new_circle;
		_new_circle = nullptr;
		return;
	}

	//_collision_system.addCircle(_new_circle->shape());
}

void Scene::resizeNewCircle(float delta)
{
	if (!_new_circle)
	{
		return;
	}

	_new_circle_size = delta + _new_circle->radius();
	_new_circle_size = (_new_circle_size > MAX_CIRCLE_SIZE) ? MAX_CIRCLE_SIZE : (_new_circle_size < MIN_CIRCLE_SIZE) ? MIN_CIRCLE_SIZE : _new_circle_size;
	_new_circle->setRadius(_new_circle_size);
}

void Scene::setNewCirclePosition(float x, float y)
{
	if (!_new_circle)
	{
		return;
	}

	CircleShape *shape = _new_circle->shape();
	shape->velocity = vec2f(x, y) - shape->position;
	_new_circle->setPosition(x, y);
}

void Scene::launchNewCircle()
{
	if (!_new_circle)
	{
		return;
	}

	_new_circle->shape()->free = true;
	_circles_on_scene.push_back(_new_circle);
	_collision_system.addCircle(_circles_on_scene.back()->shape());
	_new_circle = nullptr;
}

void Scene::clear()
{
	_collision_system.removeAllCircles();
	
	delete _new_circle;
	_new_circle = nullptr;
	_circles_on_scene.clear();
}

void Scene::resize(uint16_t width, uint16_t height)
{
	_collision_system.setDisplaySize(width, height);
}