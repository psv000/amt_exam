#pragma once

#include <new>

template<typename T>
class Vector
{
public:
	Vector();
	Vector(const Vector<T> &);
	~Vector();

	void push_back(const T &);
	void erase(size_t);
	size_t size() const;

	T operator[](size_t) const;
	T &operator[](size_t);

	T &back();

	void clear();
	
private:
	T *_array;
	size_t _size;
	size_t _capacity;
	const size_t CAPACITY_GROWTH;
};

template<typename T>
Vector<T>::Vector():
_array(nullptr),
_size(0),
_capacity(0),
CAPACITY_GROWTH(10)
{
}

template<typename T>
Vector<T>::Vector(const Vector<T> &orig)
{
}

template<typename T>
Vector<T>::~Vector()
{
	clear();
}

template<typename T>
void Vector<T>::push_back(const T &value)
{
	if (!_array || _capacity == _size)
	{
		_capacity += CAPACITY_GROWTH;
		T *new_array = (T *)malloc(sizeof(T) * _capacity);
		for (size_t i = 0; i < _size; ++i)
		{
			new( (void *)(new_array + i) ) T(_array[i]);
			_array[i].~T();
		}
		delete[] _array;
		_array = (T *)new_array;
	}

	new((void *)(_array + _size)) T(value);
	++_size;
}

template<typename T>
void Vector<T>::erase(size_t index)
{
	_array[index]~T();
	for (size_t i = index; i < _size - 1; ++i)
	{
		new (_array + i) T(_array[i + 1]);
	}
	--_size;
}

template<typename T>
size_t Vector<T>::size() const
{
	return _size;
}

template<typename T>
T Vector<T>::operator[](size_t index) const
{
	return _array[index];
}

template<typename T>
T &Vector<T>::operator[](size_t index)
{
	return _array[index];
}

template<typename T>
T &Vector<T>::back()
{
	return _array[_size - 1];
}

template<typename T>
void Vector<T>::clear()
{
	for (size_t i = 0; i < _size; ++i)
	{
		_array[i].~T();
	}
	_size = 0;
	delete[] _array;
	_array = nullptr;
}