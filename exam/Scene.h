#pragma once

#include "Circle.h"
#include "Rectangle.h"
#include "CollisionSystem.h"

#include "Vector.h"

class Scene
{
public:
	Scene();
	~Scene();
	
	void draw();

	void tryAddNewCircle		(float x, float y);
	void resizeNewCircle		(float delta);
	void setNewCirclePosition	(float x, float y);
	
	void launchNewCircle();
	
	void clear();

	void resize(uint16_t width, uint16_t height);

private:
	CollisionSystem _collision_system;

	using Circles = Vector<Circle*>;
	using Rectangles = Vector<Rect*>;

	Circles		_circles_on_scene;
	Rectangles	_rectangles;

	Circle *_new_circle;
	
	double _last_update_time;
	float _new_circle_size;
	uint16_t _width, _height;
};

