#pragma once

#include "vec2.h"

struct CircleShape
{
	CircleShape():
	mass(1.0f),
	radius(0.0f),
	restitution(10.0f),
	free(false)
	{
	}

	vec2f position;
	vec2f velocity;
	float mass;
	float radius;
	float restitution;

	bool free;
};