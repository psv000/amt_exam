#pragma once

template <typename T, size_t N>
char(&ArraySizeHelper(T(&array)[N]))[N];
#define size_of_array(array) (sizeof(ArraySizeHelper(array)))